if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[basiccustomerinformation]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[basiccustomerinformation]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[csidsetup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[csidsetup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[electronicboard]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[electronicboard]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[error]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[error]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[faxqueue]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[faxqueue]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[faxqueuefolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[faxqueuefolder]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[faxsendlog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[faxsendlog]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[faxserverinformation]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[faxserverinformation]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[faxserversetup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[faxserversetup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[linegroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[linegroup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lineroutingsetup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lineroutingsetup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[newfax]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[newfax]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[newfaxfolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[newfaxfolder]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[phonebookmanage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[phonebookmanage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[receiveassignrecord]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[receiveassignrecord]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sendmailnotice]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[sendmailnotice]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[status]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[status]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[subfaxqueuefolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[subfaxqueuefolder]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[subnewfaxfolder]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[subnewfaxfolder]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[systemparameterset]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[systemparameterset]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usergroup]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[usergroup]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[userinformation]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[userinformation]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[userlevel]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[userlevel]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FaxRecords]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[FaxRecords]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CustomerDetails]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CustomerDetails]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TransactionType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TransactionType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TrashCan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[TrashCan]
GO

CREATE TABLE [dbo].[basiccustomerinformation] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[baseno] [nvarchar] (20) NULL ,
	[name] [nvarchar] (80) NULL ,
	[company] [nvarchar] (80) NULL ,
	[documentno] [nvarchar] (80) NULL ,
	[priority] [int] NULL ,
	[coverpage] [int] NULL ,
	[coverpagefile] [nvarchar] (80) NULL ,
	[country] [nvarchar] (10) NULL ,
	[area] [nvarchar] (10) NULL ,
	[faxno] [nvarchar] (36) NULL ,
	[altcountry] [nvarchar] (10) NULL ,
	[altarea] [nvarchar] (10) NULL ,
	[altfaxno] [nvarchar] (36) NULL ,
	[dupcountry] [nvarchar] (10) NULL ,
	[duparea] [nvarchar] (10) NULL ,
	[dupfaxno] [nvarchar] (36) NULL ,
	[emailaddress] [nvarchar] (80) NULL ,
	[faxmail] [int] NULL ,
	[remark] [nvarchar] (128) NULL ,
	[address] [nvarchar] (128) NULL ,
	[telno] [nvarchar] (50) NULL ,
	[department] [nvarchar] (50) NULL ,
	[phonebookname] [int] NULL ,
	[property] [nvarchar] (10) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[csidsetup] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[csid] [nvarchar] (32) NULL ,
	[holder] [nvarchar] (384) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[electronicboard] (
	[id] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[msg] [nvarchar] (1024) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[error] (
	[code] [smallint] NULL ,
	[description] [nvarchar] (80) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[faxqueue] (
	[faxqueueindex] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[jobid] [nvarchar] (30) NULL ,
	[baseno] [nvarchar] (20) NULL ,
	[accno] [nvarchar] (20) NULL ,
	[company] [nvarchar] (128) NULL ,
	[attention] [nvarchar] (128) NULL ,
	[faxcountrycode] [nvarchar] (10) NULL ,
	[faxareacode] [nvarchar] (10) NULL ,
	[faxno] [nvarchar] (36) NULL ,
	[altfaxcountrycode] [nvarchar] (10) NULL ,
	[altfaxareacode] [nvarchar] (10) NULL ,
	[altfaxno] [nvarchar] (36) NULL ,
	[docno] [nvarchar] (20) NULL ,
	[holder] [nvarchar] (30) NULL ,
	[resendcnt] [smallint] NULL ,
	[retrycnt] [smallint] NULL ,
	[sdate] [nvarchar] (12) NULL ,
	[stime] [nvarchar] (15) NULL ,
	[sptime] [int] NULL ,
	[page] [smallint] NULL ,
	[groupname] [nvarchar] (20) NULL ,
	[faxfilename] [nvarchar] (128) NULL ,
	[orgfilename] [nvarchar] (512) NULL ,
	[giffilename] [nvarchar] (3072) NULL ,
	[snote] [nvarchar] (2048) NULL ,
	[coverpage] [nvarchar] (512) NULL ,
	[subject] [nvarchar] (512) NULL ,
	[result] [int] NULL ,
	[remark] [nvarchar] (256) NULL ,
	[mainfolderaccountno] [int] NULL ,
	[subfolderaccountno] [int] NULL ,
	[mainfoldername] [nvarchar] (50) NULL ,
	[subfoldername] [nvarchar] (50) NULL ,
	[statusflag] [int] NULL ,
	[remainresendcnt] [int] NULL ,
	[remainretrycnt] [int] NULL ,
	[priority] [smallint] NULL ,
	[reportflag] [int] NULL ,
	[reportfile] [nvarchar] (50) NULL ,
	[retrytime] [nvarchar] (50) NULL ,
	[resendtime] [nvarchar] (50) NULL ,
	[validtime] [nvarchar] (50) NULL ,
	[sendtime] [int] NULL ,
	[FaxSendEmailAck] [smallint] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[faxqueuefolder] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[holder] [nvarchar] (50) NULL ,
	[mainfoldername] [nvarchar] (128) NULL ,
	[subfolderstatus] [nvarchar] (8) NULL ,
	[subfoldername] [nvarchar] (50) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[faxsendlog] (
	[faxsendlogindex] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[jobid] [nvarchar] (30) NULL ,
	[baseno] [nvarchar] (20) NULL ,
	[accno] [nvarchar] (20) NULL ,
	[company] [nvarchar] (80) NULL ,
	[attention] [nvarchar] (30) NULL ,
	[faxcountrycode] [nvarchar] (10) NULL ,
	[faxareacode] [nvarchar] (10) NULL ,
	[faxno] [nvarchar] (36) NULL ,
	[retry] [int] NULL ,
	[sdate] [nvarchar] (10) NULL ,
	[stime] [nvarchar] (10) NULL ,
	[sptime] [int] NULL ,
	[faxservername] [nvarchar] (32) NULL ,
	[faxline] [int] NULL ,
	[faxfilename] [nvarchar] (128) NULL ,
	[orgfilename] [nvarchar] (128) NULL ,
	[snote] [nvarchar] (128) NULL ,
	[result] [int] NULL ,
	[remark] [nvarchar] (50) NULL ,
	[holder] [nvarchar] (50) NULL ,
	[tpage] [int] NULL ,
	[page] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[faxserverinformation] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[faxservername] [nvarchar] (32) NULL ,
	[ipaddress] [nvarchar] (17) NULL ,
	[faxdir] [nvarchar] (50) NULL ,
	[linecount] [int] NULL ,
	[faxgroup] [nvarchar] (50) NULL ,
	[remark] [nvarchar] (128) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[faxserversetup] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[servername] [nvarchar] (32) NULL ,
	[ip] [nvarchar] (20) NULL ,
	[faxdir] [nvarchar] (50) NULL ,
	[linecount] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[linegroup] (
	[linegroupcode] [nvarchar] (2) NOT NULL ,
	[linegroupname] [nvarchar] (50) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[lineroutingsetup] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[faxservername] [nvarchar] (32) NULL ,
	[slineno] [int] NULL ,
	[holder] [nvarchar] (256) NULL ,
	[faxno] [nvarchar] (50) NULL ,
	[linegroupcode] [nvarchar] (2) NULL ,
	[Status] [smallint] NULL  Default 0
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[newfax] (
	[newfaxindex] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[jobid] [nvarchar] (20) NULL ,
	[rdate] [nvarchar] (16) NULL ,
	[rtime] [nvarchar] (10) NULL ,
	[csid] [nvarchar] (36) NULL ,
	[ftype] [int] NULL ,
	[page] [int] NULL ,
	[faxfilename] [nvarchar] (3072) NULL ,
	[faxtifffilename] [nvarchar] (80) NULL ,
	[faxresult] [smallint] NULL ,
	[spenttime] [nvarchar] (8) NULL ,
	[dtmfcode] [nvarchar] (20) NULL ,
	[faxline] [smallint] NULL ,
	[faxservername] [nvarchar] (32) NULL ,
	[snote] [nvarchar] (256) NULL ,
	[remark] [nvarchar] (256) NULL ,
	[owner] [nvarchar] (256) NULL ,
	[dispatchstatus] [nvarchar] (8) NULL ,
	[viewstatus] [tinyint] NULL,
	[BatchNo] [char] (11) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[newfaxfolder] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[holder] [nvarchar] (50) NULL ,
	[mainfoldername] [nvarchar] (128) NULL ,
	[subfolderstatus] [nvarchar] (8) NULL ,
	[subfoldername] [nvarchar] (50) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[phonebookmanage] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[holder] [nvarchar] (50) NULL ,
	[property] [nvarchar] (20) NULL ,
	[phonebookname] [nvarchar] (80) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[receiveassignrecord] (
	[receiveassignrecordindex] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[jobid] [nvarchar] (50) NULL ,
	[holder] [nvarchar] (50) NULL ,
	[company] [nvarchar] (80) NULL ,
	[name] [nvarchar] (50) NULL ,
	[rdate] [nvarchar] (16) NULL ,
	[rtime] [nvarchar] (10) NULL ,
	[csid] [nvarchar] (36) NULL ,
	[page] [int] NULL ,
	[faxfilename] [nvarchar] (3072) NULL ,
	[faxtifffilename] [nvarchar] (80) NULL ,
	[faxresult] [int] NULL ,
	[spenttime] [nvarchar] (8) NULL ,
	[owner] [nvarchar] (50) NULL ,
	[assignstatus] [nvarchar] (8) NULL ,
	[viewstatus] [smallint]  NULL ,
	[receivedate] [nvarchar] (16) NULL ,
	[mainfoldername] [nvarchar] (50) NULL ,
	[subfoldername] [nvarchar] (50) NULL ,
	[rmemo] [nvarchar] (512) NULL ,
	[mainfolderaccountno] [int] NULL ,
	[subfolderaccountno] [int] NULL ,
	[BatchNo] [char] (11) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[sendmailnotice] (
	[sindex] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[sdate] [nvarchar] (20) NULL ,
	[stime] [nvarchar] (20) NULL ,
	[username] [nvarchar] (50) NULL ,
	[csid] [nvarchar] (36) NULL ,
	[rxfilename] [nvarchar] (80) NULL ,
	[emailaddress] [nvarchar] (50) NULL ,
	[mode] [smallint] NULL ,
	[status] [smallint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[status] (
	[code] [smallint] NULL ,
	[description] [nvarchar] (50) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[subfaxqueuefolder] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL,
	[mainfolderaccountno] [int] NULL ,
	[subfoldername] [nvarchar] (128) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[subnewfaxfolder] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL,
	[mainfolderaccountno] [int] NULL ,
	[subfoldername] [nvarchar] (128) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[systemparameterset] (
	[company] [nvarchar] (128) NULL ,
	[address] [nvarchar] (128) NULL ,
	[tel] [nvarchar] (36) NULL ,
	[fax] [nvarchar] (36) NULL ,
	[iddcall] [nvarchar] (10) NULL ,
	[ldcall] [nvarchar] (10) NULL ,
	[failresendtimes] [nvarchar] (10) NULL ,
	[failresendseparate] [nvarchar] (10) NULL ,
	[areacode] [nvarchar] (10) NULL ,
	[filterareacode] [nvarchar] (30) NULL ,
	[filterareacodelen] [nvarchar] (5) NULL ,
	[maildomainname] [nvarchar] (40) NULL ,
	[mailserveraddress] [nvarchar] (30) NULL ,
	[codeforfoip] [nvarchar] (64) NULL ,
	[foippredial] [nvarchar] (64) NULL ,
	[foipendcode] [nvarchar] (10) NULL ,
	[fontsize] [tinyint] NULL ,
	[linkaddress] [nvarchar] (50) NULL ,
	[sendmailinterval] [int] NULL ,
	[smtpcheck] [tinyint] NULL ,
	[smtpuserid] [nvarchar] (50) NULL ,
	[smtppwd] [nvarchar] (50) NULL ,
	[coverpage] [tinyint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[usergroup] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[groupname] [nvarchar] (128) NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[userinformation] (
	[sindex] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[userid] [nvarchar] (20) NULL ,
	[alias] [nvarchar] (50) NULL ,
	[password] [nvarchar] (10) NULL ,
	[faxbox] [nvarchar] (20) NULL ,
	[userlevel] [int] NULL ,
	[usergroup] [int] NULL ,
	[department] [nvarchar] (10) NULL ,
	[boxno] [nvarchar] (10) NULL ,
	[documentid] [nvarchar] (128) NULL ,
	[username] [nvarchar] (50) NULL ,
	[userphone] [nvarchar] (50) NULL ,
	[userfax] [nvarchar] (50) NULL ,
	[webfax] [nvarchar] (50) NULL ,
	[emailaddress] [nvarchar] (80) NULL ,
	[sendmailnotice] [smallint] NULL ,
	[previousip] [nvarchar] (17) NULL ,
	[usingnow] [tinyint] NULL ,
	[useruselanguage] [nvarchar] (10) NULL ,
	[webfaxfunc] [tinyint] NULL ,
	[remark] [nvarchar] (10) NULL,
        [TotalErrorCount] [int] not NULL Default (0) ,
	[Status] [int] not NULL Default (1) ,
	[FaxSendEmailAck] [smallint] NULL Default 0
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[userlevel] (
	[accountno] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[levelname] [nvarchar] (128) NULL ,
	[ssystem] [tinyint] NULL ,
	[sendrecview] [tinyint] NULL ,
	[sendrecdel] [tinyint] NULL ,
	[ldcall] [tinyint] NULL ,
	[iddcall] [tinyint] NULL ,
	[faxassign] [tinyint] NULL ,
	[urgentsend] [tinyint] NULL ,
	[pubphoneproperty] [smallint] NOT NULL ,
	[BuildFax] [tinyint] NULL Default 0,
	[CustomerDetails] [tinyint] NULL Default 0,
	[AllReceiveFax] [tinyint] NULL Default 0
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FaxRecords] (
	[RecIndex] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[SerialNo] [char] (13) NOT NULL ,
	[JobID] [nvarchar] (50) NOT NULL ,
	[PassCode] [char] (4) NULL ,
	[FaxDate] [nvarchar] (10) NULL ,
	[FaxTime] [nvarchar] (10) NULL ,
	[CustomerID] [nvarchar] (12) NULL ,
	[CustomerName] [nvarchar] (320) NULL ,
	[TransactionType] [int] NULL ,
	[DebitAcc] [nvarchar] (20) NULL ,
	[Currency] [nvarchar] (3) NULL ,
	[Amount] [money] NULL default 0,
	[ContactPerson] [nvarchar] (160) NULL ,
	[ContactTime] [smalldatetime] NULL ,
	[ContactNo] [nvarchar] (25) NULL ,
	[ForwardTo] [nvarchar] (256) NULL ,
	[Status] [tinyint] NULL ,
	[Remarks] [nvarchar] (256) NULL ,
	[faxfilename] [nvarchar] (80) NULL ,
	[BuiltBy] [nvarchar] (20) NULL ,
	[EditedBy] [nvarchar] (20) NULL ,
	[AuthorizedBy] [nvarchar] (20) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TransactionType] (
	[TransTypeID] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[TransTypeName] [nvarchar] (160) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CustomerDetails] (
	[CustomerIndex] [int] IDENTITY (1, 1) NOT FOR REPLICATION  NOT NULL ,
	[CustomerID] [nvarchar] (36) NULL ,
        [CustomerAccount] [nvarchar] (18) NULL ,
	[CustomerName] [nvarchar] (320) NULL ,
	[PassCode] [char] (4) NULL ,
	[FaxNo01] [nvarchar] (20) NULL ,
	[FaxNo02] [nvarchar] (20) NULL ,
	[ContactPerson1] [nvarchar] (160) NULL ,
	[ContactNo1] [nvarchar] (25) NULL ,
	[ContactPerson2] [nvarchar] (160) NULL ,
	[ContactNo2] [nvarchar] (25) NULL ,
	[ContactPerson3] [nvarchar] (160) NULL ,
	[ContactNo3] [nvarchar] (25) NULL ,
	[ContactPerson4] [nvarchar] (160) NULL ,
	[ContactNo4] [nvarchar] (25) NULL ,
	[ContactPerson5] [nvarchar] (160) NULL ,
	[ContactNo5] [nvarchar] (25) NULL ,
	[Remarks] [nvarchar] (256) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[TrashCan]( 
        [newfaxIndex] [int] NULL, 
        [deleteBy] [varchar](50) COLLATE Chinese_Taiwan_Stroke_CI_AS NULL, 
        [DeleteDate] [smalldatetime] NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[basiccustomerinformation] ADD 
	CONSTRAINT [DF_basiccustomerinformation_priority] DEFAULT (0) FOR [priority],
	CONSTRAINT [DF_basiccustomerinformation_coverpage] DEFAULT (0) FOR [coverpage],
	CONSTRAINT [DF_basiccustomerinformation_faxmail] DEFAULT (0) FOR [faxmail],
	CONSTRAINT [DF_basiccustomerinformation_phonebookname] DEFAULT (0) FOR [phonebookname],
	CONSTRAINT [PK_basiccustomerinformation] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[csidsetup] ADD 
	CONSTRAINT [PK_csidsetup] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[electronicboard] ADD 
	CONSTRAINT [PK_electronicboard] PRIMARY KEY  CLUSTERED 
	(
		[id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[faxqueue] ADD 
	CONSTRAINT [DF_faxqueue_page] DEFAULT (0) FOR [page],
	CONSTRAINT [DF_faxqueue_mainfolderaccountno] DEFAULT (0) FOR [mainfolderaccountno],
	CONSTRAINT [DF_faxqueue_subfolderaccountno] DEFAULT (0) FOR [subfolderaccountno],
	CONSTRAINT [DF_faxqueue_priority] DEFAULT (0) FOR [priority],
	CONSTRAINT [DF_faxqueue_sendtime] DEFAULT (0) FOR [sendtime],
	CONSTRAINT [PK_faxqueue] PRIMARY KEY  CLUSTERED 
	(
		[faxqueueindex]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[faxqueuefolder] ADD 
	CONSTRAINT [PK_faxqueuefolder] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[faxsendlog] ADD 
	CONSTRAINT [DF_faxsendlog_page] DEFAULT (0) FOR [page],
	CONSTRAINT [PK_faxsendlog] PRIMARY KEY  CLUSTERED 
	(
		[faxsendlogindex]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[faxserverinformation] ADD 
	CONSTRAINT [PK_faxserverinformation] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[faxserversetup] ADD 
	CONSTRAINT [PK_faxserversetup] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[linegroup] ADD 
	CONSTRAINT [PK_linegroup] PRIMARY KEY  CLUSTERED 
	(
		[linegroupcode]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[lineroutingsetup] ADD 
	CONSTRAINT [DF_lineroutingsetup_linegroupcode] DEFAULT (0) FOR [linegroupcode],
	CONSTRAINT [PK_lineroutingsetup] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[newfax] ADD 
	CONSTRAINT [DF_newfax_page] DEFAULT (0) FOR [page],
	CONSTRAINT [DF_newfax_viewstatus] DEFAULT (0) FOR [viewstatus],
	CONSTRAINT [PK_newfax] PRIMARY KEY  CLUSTERED 
	(
		[newfaxindex]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[newfaxfolder] ADD 
	CONSTRAINT [PK_newfaxfolder] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[phonebookmanage] ADD 
	CONSTRAINT [PK_phonebookmanage] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[receiveassignrecord] ADD 
	CONSTRAINT [DF_receiveassignrecord_page] DEFAULT (0) FOR [page],
	CONSTRAINT [DF_receiveassignrecord_mainfolderaccountno] DEFAULT (0) FOR [mainfolderaccountno],
	CONSTRAINT [DF_receiveassignrecord_subfolderaccountno] DEFAULT (0) FOR [subfolderaccountno],
	CONSTRAINT [PK_receiveassignrecord] PRIMARY KEY  CLUSTERED 
	(
		[receiveassignrecordindex]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[sendmailnotice] ADD 
	CONSTRAINT [DF_sendmailnotice_status] DEFAULT (0) FOR [status],
	CONSTRAINT [PK_sendmailnotice] PRIMARY KEY  CLUSTERED 
	(
		[sindex]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[subfaxqueuefolder] ADD 
	CONSTRAINT [PK_subfaxqueuefolder] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[subnewfaxfolder] ADD 
	CONSTRAINT [PK_subnewfaxfolder] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[systemparameterset] ADD 
	CONSTRAINT [DF_systemparameterset_fontsize] DEFAULT (10) FOR [fontsize],
	CONSTRAINT [DF_systemparameterset_sendmailinterval] DEFAULT (5) FOR [sendmailinterval],
	CONSTRAINT [DF_systemparameterset_smtpcheck] DEFAULT (0) FOR [smtpcheck],
	CONSTRAINT [DF_systemparameterset_coverpage] DEFAULT (0) FOR [coverpage]
GO

ALTER TABLE [dbo].[usergroup] ADD 
	CONSTRAINT [PK_usergroup] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[userinformation] ADD 
	CONSTRAINT [DF_userinformation_sendmailnotice] DEFAULT (0) FOR [sendmailnotice],
	CONSTRAINT [DF_userinformation_usingnow] DEFAULT (0) FOR [usingnow],
	CONSTRAINT [DF_userinformation_useruselanguage] DEFAULT (N'Eng') FOR [useruselanguage],
	CONSTRAINT [DF_userinformation_webfaxfunc] DEFAULT (1) FOR [webfaxfunc],
	CONSTRAINT [PK_userinformation] PRIMARY KEY  CLUSTERED 
	(
		[sindex]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[userlevel] ADD 
	CONSTRAINT [PK_userlevel] PRIMARY KEY  CLUSTERED 
	(
		[accountno]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[FaxRecords] ADD 
	CONSTRAINT [PK_FaxRecords] PRIMARY KEY  CLUSTERED 
	(
		[RecIndex]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[TransactionType] ADD 
	CONSTRAINT [PK_TransactionType] PRIMARY KEY  CLUSTERED 
	(
		[TransTypeID]
	)  ON [PRIMARY]
GO

ALTER TABLE [dbo].[CustomerDetails] ADD
	CONSTRAINT [PK_CustomerDetails] PRIMARY KEY  CLUSTERED 
	(
		[CustomerIndex]
	)  ON [PRIMARY] 
GO


INSERT INTO error(code,description) VALUES (0,'Success')
GO
INSERT INTO error(code,description) VALUES (43,'System Busy')
GO
INSERT INTO error(code,description) VALUES (101,'FAX Card no response')
GO
INSERT INTO error(code,description) VALUES (102,'超過連線時間仍無法連線')
GO
INSERT INTO error(code,description) VALUES (103,'檔案格式錯誤')
GO
INSERT INTO error(code,description) VALUES (104,'使用者中斷')
GO
INSERT INTO error(code,description) VALUES (105,'無法與對方傳真機連線')
GO
INSERT INTO error(code,description) VALUES (106,'多頁錯誤')
GO
INSERT INTO error(code,description) VALUES (107,'線路不通')
GO
INSERT INTO error(code,description) VALUES (108,'忙線中')
GO
INSERT INTO error(code,description) VALUES (109,'傳送完畢但無法確認')
GO
INSERT INTO error(code,description) VALUES (120,'傳真資料有誤')
GO
INSERT INTO error(code,description) VALUES (121,'傳真號碼錯誤')
GO
INSERT INTO error(code,description) VALUES (122,'重傳次數錯誤')
GO
INSERT INTO error(code,description) VALUES (123,'系統無法傳送')
GO
INSERT INTO error(code,description) VALUES (124,'交易代碼錯誤')
GO
INSERT INTO error(code,description) VALUES (999,'使用者停傳')
GO

INSERT INTO status(code,description) VALUES (1,'待傳件')
GO
INSERT INTO status(code,description) VALUES (2,'傳送中')
GO
INSERT INTO status(code,description) VALUES (3,'成功')
GO
INSERT INTO status(code,description) VALUES (4,'失敗')
GO
INSERT INTO status(code,description) VALUES (5,'待轉檔')
GO
INSERT INTO status(code,description) VALUES (6,'待預覽')
GO

INSERT INTO linegroup(linegroupcode,linegroupname) VALUES ('0','Public')
GO

INSERT INTO systemparameterset(company,address,tel,fax,iddcall,ldcall,failresendtimes,failresendseparate,areacode,filterareacode,filterareacodelen,maildomainname,mailserveraddress,codeforfoip,foippredial,foipendcode,fontsize,linkaddress,sendmailinterval,smtpcheck,smtpuserid,smtppwd,coverpage) VALUES ('Chinatrust-SG','','','','00','0','3','60','02','02','2','','','1111','0','#',12,'',5,0,'','',1)
GO

INSERT INTO userlevel(levelname,ssystem,sendrecview,sendrecdel,ldcall,iddcall,faxassign,urgentsend,pubphoneproperty,BuildFax,CustomerDetails,AllReceiveFax) VALUES ('Administrators',1,1,1,1,1,1,1,3,2,2,1)
GO

INSERT INTO userlevel(levelname,ssystem,sendrecview,sendrecdel,ldcall,iddcall,faxassign,urgentsend,pubphoneproperty,BuildFax,CustomerDetails,AllReceiveFax) VALUES ('Managers',0,1,1,1,1,1,1,3,3,2,1)
GO

INSERT INTO userlevel(levelname,ssystem,sendrecview,sendrecdel,ldcall,iddcall,faxassign,urgentsend,pubphoneproperty,BuildFax,CustomerDetails,AllReceiveFax) VALUES ('SG receiver',0,0,0,0,0,1,0,0,1,2,0)
GO

INSERT INTO userlevel(levelname,ssystem,sendrecview,sendrecdel,ldcall,iddcall,faxassign,urgentsend,pubphoneproperty,BuildFax,CustomerDetails,AllReceiveFax) VALUES ('SG maker',0,0,0,0,0,0,0,0,2,1,0)
GO

INSERT INTO userlevel(levelname,ssystem,sendrecview,sendrecdel,ldcall,iddcall,faxassign,urgentsend,pubphoneproperty,BuildFax,CustomerDetails,AllReceiveFax) VALUES ('SG CS',0,0,0,0,0,0,0,1,1,1,1)
GO

INSERT INTO userlevel(levelname,ssystem,sendrecview,sendrecdel,ldcall,iddcall,faxassign,urgentsend,pubphoneproperty,BuildFax,CustomerDetails,AllReceiveFax) VALUES ('sysmgt',1,0,0,0,0,0,0,0,0,0,0)
GO

INSERT INTO userlevel(levelname,ssystem,sendrecview,sendrecdel,ldcall,iddcall,faxassign,urgentsend,pubphoneproperty,BuildFax,CustomerDetails,AllReceiveFax) VALUES ('SG checker',0,0,0,0,0,0,0,3,3,1,1)
GO

INSERT INTO usergroup(groupname) VALUES ('CTCBSG-OP')
GO

INSERT INTO usergroup(groupname) VALUES ('CTCBSG')
GO

INSERT INTO usergroup(groupname) VALUES ('CTCBTP')
GO

INSERT INTO usergroup(groupname) VALUES ('Management')
GO

INSERT INTO userInformation(userid,alias,password,faxbox,userlevel,usergroup,department,boxno,documentid,username,userphone,userfax,webfax,emailaddress,sendmailnotice,previousip,usingnow,useruselanguage,webfaxfunc,remark) VALUES ('admin','admin','admin','',1,4,'','','','','','','','',1,'',0,'Eng',1,'')
GO

INSERT INTO userInformation(userid,alias,password,faxbox,userlevel,usergroup,department,boxno,documentid,username,userphone,userfax,webfax,emailaddress,sendmailnotice,previousip,usingnow,useruselanguage,webfaxfunc,remark) VALUES ('bruce','bruce','bruce','',1,4,'','','','','','','','',1,'',0,'Eng',1,'')
GO

INSERT INTO userInformation(userid,alias,password,faxbox,userlevel,usergroup,department,boxno,documentid,username,userphone,userfax,webfax,emailaddress,sendmailnotice,previousip,usingnow,useruselanguage,webfaxfunc,remark) VALUES ('maker1','Dep & Rem','maker1','',4,1,'','','','','','','','',1,'',0,'Eng',1,'')
GO

INSERT INTO userInformation(userid,alias,password,faxbox,userlevel,usergroup,department,boxno,documentid,username,userphone,userfax,webfax,emailaddress,sendmailnotice,previousip,usingnow,useruselanguage,webfaxfunc,remark) VALUES ('bmaker','Bills','bmaker','',4,1,'','','','','','','','',1,'',0,'Eng',1,'')
GO

INSERT INTO userInformation(userid,alias,password,faxbox,userlevel,usergroup,department,boxno,documentid,username,userphone,userfax,webfax,emailaddress,sendmailnotice,previousip,usingnow,useruselanguage,webfaxfunc,remark) VALUES ('checker','checker','checker','',7,1,'','','','','','','','',1,'',0,'Eng',1,'')
GO

INSERT INTO userInformation(userid,alias,password,faxbox,userlevel,usergroup,department,boxno,documentid,username,userphone,userfax,webfax,emailaddress,sendmailnotice,previousip,usingnow,useruselanguage,webfaxfunc,remark) VALUES ('receiver','assign','receiver','',3,1,'','','','','','','','',1,'',0,'Eng',1,'')
GO

INSERT INTO userInformation(userid,alias,password,faxbox,userlevel,usergroup,department,boxno,documentid,username,userphone,userfax,webfax,emailaddress,sendmailnotice,previousip,usingnow,useruselanguage,webfaxfunc,remark) VALUES ('call','CSR','call','',5,3,'','','','','','','','',1,'',0,'Eng',1,'')
GO

INSERT INTO userInformation(userid,alias,password,faxbox,userlevel,usergroup,department,boxno,documentid,username,userphone,userfax,webfax,emailaddress,sendmailnotice,previousip,usingnow,useruselanguage,webfaxfunc,remark) VALUES ('re1','reveive','re1','',3,1,'','','','','','','','',1,'',0,'Eng',1,'')
GO

INSERT INTO TransactionType(TransTypeName) VALUES ('Deposit')
GO

INSERT INTO TransactionType(TransTypeName) VALUES ('Remittance')
GO

INSERT INTO TransactionType(TransTypeName) VALUES ('Import Bills')
GO

INSERT INTO TransactionType(TransTypeName) VALUES ('Export Bills')
GO
