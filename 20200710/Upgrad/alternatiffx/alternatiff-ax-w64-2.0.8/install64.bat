@echo off

rem Change to the directory containing this batch file.
cd "%~dp0"

if not exist alttiff64.ocx goto bad
start regsvr32 alttiff64.ocx
goto end
:bad
echo Error: Cannot find alttiff64.ocx.
echo Note: Do not run install64.bat directly from your unzip program.
echo You must create a folder and unzip everything into it.
echo Note: The current folder is:
cd
pause
:end

